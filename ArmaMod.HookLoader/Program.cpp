#include <Windows.h>
#include <iostream>

typedef void (__stdcall* RVExtension)(char *output, int outputSize, const char *function);

void InvokeFunction(RVExtension rvExtension, const char* function)
{
	static const int outputSize= 4096;
	char output[outputSize];
	rvExtension(output, outputSize, function);
	printf("Function: %s\n", function);
	printf("Mod output: %s\n\n", output);
}
void main()
{
	auto hookInstance = LoadLibrary("ArmaMod.Hook.dll");	

	auto procAddress = GetProcAddress(hookInstance, "_RVExtension@12");

	auto rvExtension = reinterpret_cast<RVExtension>(procAddress);
	
	InvokeFunction(rvExtension, "SayHello");
	InvokeFunction(rvExtension, "DoSomething");
	InvokeFunction(rvExtension, "NotAFunction");

	std::cout << "Press any key to continue . . ." << std::endl;

	getchar();
}
