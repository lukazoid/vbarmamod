#include <msclr\marshal.h>

using namespace System;
using namespace msclr::interop;

extern "C" _declspec(dllexport) void __stdcall RVExtension(char *output, int outputSize, const char *function)
{
	// Marshal from unmanaged -> managed
	auto result = ArmaMod::ModEntry::RVExtension(outputSize - 1, marshal_as<String^>(function));

	marshal_context marshalContext;
	
	// Marshal from managed -> unmanaged
	strcpy_s(output, outputSize, marshalContext.marshal_as<const char *>(result));
};