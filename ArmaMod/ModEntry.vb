﻿Public Class ModEntry
    Public Shared Function RVExtension(outputSize As Integer, [function] As String) As String
        Dim output As String
        Select Case [function]
            Case "DoSomething"
                output = "I did something"
            Case "SayHello"
                output = "Hello from your VB.NET mod!"
            Case Else
                output = String.Format("I cannot perform function '{0}'", [function])
        End Select
        
        If (output.Length > outputSize) Then
            Return output.Substring(0, outputSize)
        End If

        Return output
    End Function
End Class
